from keras import models
from keras import layers
from keras.preprocessing.image import ImageDataGenerator
from keras.preprocessing import image
from keras import optimizers
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np

# Augment data
train_datagen = ImageDataGenerator(rescale=1./255, rotation_range=0.2, width_shift_range=0.2, height_shift_range=0.2, shear_range=0.2, horizontal_flip=True, fill_mode="nearest")
val_datagen = ImageDataGenerator(rescale=1./255)

# Flow from directory
train_generator = train_datagen.flow_from_directory("/content/train/", target_size=(150,150), batch_size=64, class_mode="categorical")
val_generator = val_datagen.flow_from_directory("/content/val/", target_size=(150,150), batch_size=64, class_mode="categorical")

# Model architecture
model = models.Sequential()
model.add(layers.Conv2D(128, (3,3), activation="relu", input_shape=(150,150,3)))
model.add(layers.Conv2D(128, (3,3), activation="relu"))
model.add(layers.MaxPooling2D(2,2))
model.add(layers.Conv2D(64, (3,3), activation="relu"))
model.add(layers.MaxPooling2D(2,2))
model.add(layers.Conv2D(32, (3,3), activation="relu"))
model.add(layers.MaxPooling2D(2,2))
model.add(layers.Flatten())
model.add(layers.Dense(500, activation="relu"))
model.add(layers.Dense(10, activation="softmax"))
#model.summary()

model.compile(loss="categorical_crossentropy", optimizer=optimizers.RMSprop(lr=1e-4), metrics=["acc"])

history = model.fit_generator(train_generator, steps_per_epoch=550, epochs=40, validation_data=val_generator, validation_steps=83)

#save model
#model.save("simpsons_classifier_1.h5")


# Graph efficiency
val_acc = history.history["val_acc"]
acc = history.history["acc"]
epochs = range(1, len(acc)+1)

plt.plot(epochs, val_acc, "green", label="Validation")
plt.plot(epochs, acc, "blue", label="Train")
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
plt.show()


# Predict image
img = image.load_img("/content/val/marge_simpson/pic_0027.jpg", target_size=(150,150))
img_tensor = image.img_to_array(img)
img_tensor = np.expand_dims(img_tensor, axis=0)
img_tensor /= 255.

pred = model.predict_classes(img_tensor)
pred[0]
for key, value in val_generator.class_indices.items():
  if pred[0] == value:
    prediction = key

print(prediction)

plt.text(10, 10, "%s"%prediction, fontsize=15)
plt.imshow(img_tensor[0])

