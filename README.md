# Simpsons Classifier

# Architecture
<h3>simpson_classifier_1.h5</h3>
    The model is a sequential convolutional neural network with ten layers displayed in the following format

    (layers.Conv2D(128, (3,3), activation="relu", input_shape=(150,150,3)))
    (layers.Conv2D(128, (3,3), activation="relu"))
    (layers.MaxPooling2D(2,2))
    (layers.Conv2D(64, (3,3), activation="relu"))
    (layers.MaxPooling2D(2,2))
    (layers.Conv2D(32, (3,3), activation="relu"))
    (layers.MaxPooling2D(2,2))
    (layers.Flatten())
    (layers.Dense(500, activation="relu"))
    (layers.Dense(10, activation="softmax"))

    Data augmentation was used and the simpson_classifier_1.h5 brain was trained using 4x the dataset. target=(150,150)

    ImageDataGenerator(rescale=1./255, rotation_range=0.2, width_shift_range=0.2, height_shift_range=0.2, shear_range=0.2, horizontal_flip=True, fill_mode="nearest")

# Classes

    There are ten classes. The dataset can be found on kaggle, but only labels with > 1000 samples were used. (500 samples moved to validation directory)

    bart_simpson
    charles_montgomery_burns
    homer_simpson
    krusty_the_clown
    lisa_simpson
    marge_simpson
    milhouse_van_houten
    moe_szyslak
    ned_flanders
    principal_skinner

https://www.kaggle.com/alexattia/the-simpsons-characters-dataset
  
# Accuracy
  * simpson_classifier_0.h5 = 85%
  * simpson_classifier_1.h5 = 91%